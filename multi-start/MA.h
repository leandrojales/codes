#include <iostream> 
#include <algorithm> 
#include <cmath> 
#include <fstream>
#include <iterator>
#include <sstream> 
#include <string>
#include <vector> 

#define __NODES	0
#define __SOL	1
#define __TYPE	2
#define __SWAP(_X,_Y) int _A=_X; _X=_Y; _Y=_A;

using namespace std;

typedef pair<float,float> PAIR;

class PCV_INFO{
private:
	/****************
	 * Init data	*
	 ****************/
	string aux;	// Aux variable
	float num;	// Aux variable
	vector< float > info;	// Vector with N SOL TIPO
	vector< PAIR > XY;	// Vector of all XY's
	vector< vector<float> > matrix;	// Adj matrix
	ifstream file;	// File with data
	istream_iterator<string> it;	// Pointer to data in file
	istream_iterator<string> eos;	// Pointer to end of file

public:
	void getInfo( char *arc ){

		info.assign(3,0);	// Assign the size of info
		file.open( arc );	// Open a file
		it = file;		// Init a iterator

		if( it == eos ) exit(1); // If file empty
	
		/************************
		 * Converts the string  *
		 * (of file) to integer *
		 ***********************/
		/** Get #nodes, the solution, the type of info **/
		for( int i = 0 ; i < 3 ; i++ ){
			aux = (*it++).substr( (*it).find("=")+1 );
			if( aux == "" ){ *it++; aux = "-1"; }
			istringstream transl( aux );
			transl >> info[ i ];
		}
		it = getline( file,aux );	// it go to next line

		XY.assign( (int)info[ __NODES ], make_pair( 0 , 0 ) );	// assing the size of __NODES
		matrix.resize( info[ __NODES ] );

		/* Treat the types of file */
		switch( (int)info[ __TYPE ] ){
			case 1: getInfo_type1(); break;
			case 2: getInfo_type2(); break;
			case 3: getInfo_type3(); break;
		}
	}

	void getInfo_type1(){
		// Gets nodes of file
		while( it != eos ){
			for( int i = 0 ; i < info[ __NODES ] ; i++ )
				for( int j = 0 ; j < info[ __NODES ] - i - 1 ; j++ ){
					istringstream transl( *it++ ); float auxT1 = 0;
					transl >> auxT1;
					fill_m( i,auxT1 );
				}
		}
//		print_m();
	}
	void getInfo_type2(){ cout << "There's no data with the type 2" << endl; exit(1); }

	void getInfo_type3(){
		// Get values of XY
		int count = 0;
		while( ++it != eos ){
			// Transl string to float and assign the values of X's and Y's to vector XY
			istringstream transl;
			pair<string,string> auxT3;
			auxT3 =  make_pair( *it++,*it++ );
			// TREAT THE EXCEPTION if X == (num1)e+(expo) <==> (num1)*(10^expo)
			if( (auxT3.first).find("e+") != -1 ){
				auxT3.first = exception( auxT3.first );
				auxT3.second = exception( auxT3.second );
			}
			transl.str( auxT3.first  ); transl >> (XY[ count ]).first; transl.clear();
			transl.str( auxT3.second ); transl >> (XY[ count ]).second;
			count++;
		}
		calc( XY );	// calc(): the distance between points
//		print_m();

	}

	/** TREAT THE EXCEPTION if X == (num1)e+(expo) <==> (num1)*(10^expo) **/
	string exception( string num ){
		float number = 0, base = 10, expo = 0;
		istringstream transl;
		string aux;
		int pos = num.find( "e+" );
		// Gets (num1)
		aux = num.substr( 0,pos );
		transl.str( aux ); transl >> number; transl.clear();
		// Gets (expo)
		aux = num.substr( pos+1 );
		transl.str( aux ); transl >> expo; transl.clear();
		number = number*( pow( 10, expo ) );
		ostringstream con; con << number;
		return con.str();
	}

	/** Calc the distance of all points **/
	void calc( vector< PAIR > points ){
		for( int i = 0 ; i < points.size() ; i++ ){
			for( int j = i + 1 ; j < points.size() ; j++ ){
				fill_m( i, distance( points[ i ], points[ j ] ) );
			}
		}
	}
	/** Calc dist between two points **/
	float distance( PAIR P1, PAIR P2){
		float dist = 0;
		// dist^2 = (x1-x2)^2 + (y1-y2)^2
		dist = pow( (P1.first - P2.first), 2 ) + pow( (P1.second - P2.second), 2 );
		dist = sqrt( dist );
		return dist;
	}

	/** Fill the matrix with data **/
	void fill_m( int pos, float value ){
		matrix[ pos ].push_back( value );
	}

	/** Print the matrix **/
	void print_m(){
		cout << "Printing matrix" << endl;
		for( int i = 0 ; i < matrix.size() ; i++ ){
			for( int j = 0 ; j < matrix[i].size() ; j++ )
				cout << matrix[ i ][ j ] << " "; cout << endl;}
	}

	/** Get the value into my matrix **/
	float getValue( int i , int j ){
		if( i == j ) return 0;
		if( i > j ) { __SWAP(i,j) };
		return matrix[ i ][ j - i - 1 ];
	}

	/** Return the size of matrix - to cod1.c **/
	int size_matrix(){
		return matrix.size();
	}
	
	int Info( int information ){
		if( information >= 0 && information < 3 )
			return info[ information ];
		cout << "Error!!! Number invalid!" << endl;
		exit(1);
	}

	/** Transformed the inversed matrix in completed matrix and return it - to cod1.c **/
	float **getMatrix(){
		float ** M_ret = new float *[ matrix.size() ];
		for( int i = 0 ; i < matrix.size() ; i++ ) M_ret[ i ] = new float [ matrix.size() ];
		for( int i = 0 ; i < matrix.size() ; i++ )
		for( int j = 0 ; j < matrix.size() ; j++ )
				M_ret[ i ][ j ] = getValue( i,j );
		return M_ret;
	}
};

class PCV_MSA{
public:
};
